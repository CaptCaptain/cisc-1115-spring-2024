import java.util.*;

public class ChangeCalculator {
    public static String Whitespace = "                                          ";
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.print("Enter total price including cents to be paid: ");

        double totalPrice = in.nextDouble();

        System.out.print("Enter the cash the customer is paying: ");

        double pricePaid = in.nextDouble();

        Boolean notFullyPaid = pricePaid < totalPrice;

        Boolean fullyPaid = pricePaid == totalPrice;

        Boolean overPaid = pricePaid - totalPrice > 100;

        System.out.println("");

        System.out.println(String.format("%sExact Change Calculation", Whitespace.substring(0, 6)));

        System.out.println(String.format("Total price to be paid: $%s", totalPrice));

        System.out.println(String.format("%sThe cash received: $%s", Whitespace.substring(0, 5), pricePaid));

        if (notFullyPaid) {
            System.out.println("Need more cash from the customer.");
        } else if (fullyPaid) {
            System.out.println("Exact payment. No changes.");
        } else if (overPaid) {
            System.out.println("Too much cash!");
        } else {
            double changeDue = pricePaid - totalPrice;
            int changeDueInCents = (int) Math.round(changeDue * 100);

            final int TWENTY_DOLLARS_IN_CENTS = 2000;
            final int FIVE_DOLLARS_IN_CENTS = 500;
            final int TWO_DOLLARS_IN_CENTS = 200;
            final int ONE_DOLLARS_IN_CENTS = 100;

            final int QUARTERS_IN_CENTS = 25;
            final int DIMES_IN_CENTS = 10;
            final int NICKLES_IN_CENTS = 5;
            final int PENNIES_IN_CENTS = 1;

            final int[] billDenominations = {TWENTY_DOLLARS_IN_CENTS, FIVE_DOLLARS_IN_CENTS, TWO_DOLLARS_IN_CENTS, ONE_DOLLARS_IN_CENTS};
            final int[] centDenominations = {QUARTERS_IN_CENTS, DIMES_IN_CENTS, NICKLES_IN_CENTS, PENNIES_IN_CENTS};

            final String[] billStrings = {
                String.format("%s$20 bill:", Whitespace.substring(0, 14)),
                String.format("%s$5 bill:", Whitespace.substring(0, 15)),
                String.format("%s$2 bill:", Whitespace.substring(0, 15)),
                String.format("%s$1 bill:", Whitespace.substring(0, 15)),
            };
            final String[] centStrings = {
                String.format("%sQuarter:", Whitespace.substring(0, 15)),
                String.format("%sDime:", Whitespace.substring(0, 18)),
                String.format("%sNickle:", Whitespace.substring(0, 16)),
                String.format("%sPenny:", Whitespace.substring(0, 17)),
            };

            System.out.println("");

            System.out.println(String.format("%sGive to the customer:", Whitespace.substring(0, 2)));

            for (int i = 0; i < billDenominations.length; i++) {
                int due = changeDueInCents / billDenominations[i];
                changeDueInCents %= billDenominations[i];
                
                System.out.println(String.format(billStrings[i] + " " + "%s", due));
            }

            System.out.println("");

            for (int i = 0; i < centDenominations.length; i++) {
                int due = changeDueInCents / centDenominations[i];
                changeDueInCents %= centDenominations[i];
                
                System.out.println(String.format(centStrings[i] + " " + "%s", due));
            }
        }
    }
}

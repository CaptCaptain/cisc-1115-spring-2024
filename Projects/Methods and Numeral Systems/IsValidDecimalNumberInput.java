public class IsValidDecimalNumberInput {
    public static boolean isValidDecimalNumberInput(String input) {
        input = input.trim();

        if (input.length() > 18) {
            return false;
        }

        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);

            if (!Character.isDigit(c)) {
                return false;
            }
        }

        return true;
    }
}

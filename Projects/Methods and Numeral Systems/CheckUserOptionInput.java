public class CheckUserOptionInput {
    public static int checkUserOptionInput(String input) {
        input = input.trim();

        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);

            if (!Character.isDigit(c)) {
                return -1;
            }
        }

        int option = Integer.parseInt(input);

        if (option < 0 || option > 2) {
            return -1;
        }

        return option;
    }
}

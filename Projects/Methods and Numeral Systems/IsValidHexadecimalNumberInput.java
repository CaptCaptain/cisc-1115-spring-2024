public class IsValidHexadecimalNumberInput {
    public static boolean isValidHexadecimalNumberInput(String input) {
        input = input.trim();

        if (input.length() > 15) {
            return false;
        }

        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);

            if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'F')) {
                continue;
            } else {
                return false;
            }
        }

        return true;
    }
}

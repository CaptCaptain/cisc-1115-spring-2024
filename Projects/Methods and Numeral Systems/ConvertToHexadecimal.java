public class ConvertToHexadecimal {

    public static String convertToHexadecimal(String decText) {
        if (decText.length() > 18) {
            return "";
        }

        for (int i = 0; i < decText.length(); i++) {
            char c = decText.charAt(i);

            if (c >= '0' && c <= '9') {
                continue;
            }

            return "";
        }

        String hexText = "";

        for (long decNumber = Long.parseLong(decText); decNumber > 0; decNumber /= 16) {
            long d = decNumber % 16;

            if (d >= 0 && d <= 9) {
                hexText = (char) ('0' + d) + hexText;
            } else {
                hexText = (char) ('A' + d - 10) + hexText;
            }
        }

        return hexText;
    }

    public static void main(String[] args) {
        System.out.println(convertToHexadecimal("123456789012345678"));
    }
}
public class ConvertToDecimal {
    public static long convertToDecimal(String hexText) {
        long d = 0;

        if (hexText.length() > 15) {
            return d;
        }

        for (int i = 0; i < hexText.length(); i++) {
            char c = hexText.charAt(i);
            long coefficient = 0;
            long base = 16;
            long exponent = hexText.length() - i - 1;

            if (c >= '0' && c <= '9') {
                coefficient = 9 - ('9' - c);
            } else if (c >= 'A' && c <= 'F') {
                coefficient = 15 - ('F' - c);
            } else {
                return d;
            }

            d = d + (long) Math.round(coefficient * Math.pow(base, exponent));
        }

        return d;
    }

    public static void main(String[] args) {
        System.out.println(convertToDecimal("FFFFFFFFFFFFFFF"));
    }
}

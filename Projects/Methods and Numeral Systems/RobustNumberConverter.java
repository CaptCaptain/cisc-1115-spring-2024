import java.util.*;

public class RobustNumberConverter {
    public static int checkUserOptionInput(String input) {
        input = input.trim();

        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);

            if (!Character.isDigit(c)) {
                return -1;
            }
        }

        int option = Integer.parseInt(input);

        if (option < 0 || option > 2) {
            return -1;
        }

        return option;
    }

    public static boolean isValidDecimalNumberInput(String input) {
        input = input.trim();

        if (input.length() > 18) {
            return false;
        }

        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);

            if (!Character.isDigit(c)) {
                return false;
            }
        }

        return true;
    }

    public static boolean isValidHexadecimalNumberInput(String input) {
        input = input.trim();

        if (input.length() > 15) {
            return false;
        }

        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);

            if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'F')) {
                continue;
            } else {
                return false;
            }
        }

        return true;
    }

    public static long convertToDecimal(String hexText) {
        long d = 0;

        if (hexText.length() > 15) {
            return d;
        }

        for (int i = 0; i < hexText.length(); i++) {
            char c = hexText.charAt(i);
            long coefficient = 0;
            long base = 16;
            long exponent = hexText.length() - i - 1;

            if (c >= '0' && c <= '9') {
                coefficient = 9 - ('9' - c);
            } else if (c >= 'A' && c <= 'F') {
                coefficient = 15 - ('F' - c);
            } else {
                return d;
            }

            d = d + (long) Math.round(coefficient * Math.pow(base, exponent));
        }

        return d;
    }

    public static String convertToHexadecimal(String decText) {
        if (decText.length() > 18) {
            return "";
        }

        for (int i = 0; i < decText.length(); i++) {
            char c = decText.charAt(i);

            if (c >= '0' && c <= '9') {
                continue;
            }

            return "";
        }

        String hexText = "";

        for (long decNumber = Long.parseLong(decText); decNumber > 0; decNumber /= 16) {
            long d = decNumber % 16;

            if (d >= 0 && d <= 9) {
                hexText = (char) ('0' + d) + hexText;
            } else {
                hexText = (char) ('A' + d - 10) + hexText;
            }
        }

        return hexText;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        int option = 0;

        while (true) {
            while (true) {
                System.out.printf(
                    "%s\n%s\n%s\n%s",
                    "1. Convert hexadecimal number to decimal",
                    "2. Decimal decimal number to hexadecimal",
                    "0. Exit",
                    "Enter option (0, 1, or 2) and hit enter: "
                );

                String input = "";

                if (in.hasNextLine()) {
                    input = in.nextLine();
                }
    
                option = checkUserOptionInput(input);
    
                if (option == -1) {
                    continue;
                }
    
                break;
            }
    
            if (option == 0) {
                System.out.println("Exit the program");
                return;
            }
    
            if (option == 2) {
                String decInput = "";

                while (decInput.equals("")) {
                    System.out.print("Enter an valid decimal number (no more than 18 digits): ");

                    if (in.hasNextLine()) {
                        decInput = in.nextLine();
                    }

                    if (isValidDecimalNumberInput(decInput)) {
                        break;
                    }

                    decInput = "";
                }

                System.out.printf("Decimal number %s is hexadecimal number %s.\n", decInput, convertToHexadecimal(decInput));
            } else if (option == 1) {
                String hexInput = "";

                while (hexInput.equals("")) {
                    System.out.print("Enter an valid hexadecimal number (no more than 15 digits): ");

                    if (in.hasNextLine()) {
                        hexInput = in.nextLine();
                    }

                    if (isValidHexadecimalNumberInput(hexInput)) {
                        break;
                    }

                    hexInput = "";
                }

                System.out.printf("Hexadecimal number %s is decimal number %s.\n", hexInput, convertToDecimal(hexInput));
            }
        }
    }
}

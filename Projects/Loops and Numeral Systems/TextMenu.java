import java.util.*;

public class TextMenu {
    public static void outputDebugMessage(String msg) {
        System.out.printf("DEBUG: " + msg);
    }

    public static void promptMenu() {
        System.out.printf(
            "%s\n%s\n%s\n%s",
            "1. Convert octal number to decimal",
            "2. Decimal decimal number to octal",
            "0. Exit",
            "Enter option (0, 1, or 2) and hit enter: "
        );
    }

    public static int getInput(Scanner scanner) {
        int option = -1;

        while (option == -1) {
            promptMenu();

            String input = scanner.nextLine().trim();

            outputDebugMessage(
                String.format("user entered <%s>" + "\n", input)
            );

            if (!input.matches("^\\d+")) {
                continue;
            }
            
            option = Integer.parseInt(input);

            if (option < 0 || option > 2) {
                option = -1;
                continue;
            }

            if (option > 0) {
                outputDebugMessage(
                    String.format("the user input is <%d>, a valid option" + "\n", option)
                );
            }
        }

        return option;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        int input = -1;

        while (input != 0) {
            input = getInput(in);
    
            switch (input) {
                case 0:
                    System.out.println("User selected option 0 to exit the program");
                    break;
                case 1:
                    System.out.println("User selected option 1 to convert octal number to decimal");
                    break;
                case 2:
                    System.out.println("User selected option 2 to convert decimal number to octal");
                    break;
            }
        }
    }
}

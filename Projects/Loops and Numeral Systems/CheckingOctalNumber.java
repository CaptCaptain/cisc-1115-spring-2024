public class CheckingOctalNumber {
    public static void outputDebugMessage(String msg) {
        System.out.printf("DEBUG: " + msg);
    }

    public static Boolean isOctalDigitValid(char c) {
        Boolean isOctalDigit = c >= '0' && c <= '7';

        return isOctalDigit;
    }

    public static Boolean isOctalNumberIsValid(String octalString) {
        Boolean octalNumberValid = true;

        outputDebugMessage(
            String.format(
                "user's input is <%s>\n",
                octalString
            )
        );

        octalString = octalString.trim();

        outputDebugMessage(
            String.format(
                "trimmed input is <%s>\n",
                octalString
            )
        );

        outputDebugMessage(
            String.format(
                "the length of the trimmed input is <%d>\n",
                octalString.length()
            )
        );

        if (octalString.length() == 0) {
            return false;
        }

        if (octalString.length() > 4) {
            return false;
        }

        for (int i = 0; i < octalString.length(); i++) {
            outputDebugMessage(
                String.format(
                    "character at index <%d> is <%s>\n",
                    i, octalString.charAt(i)
                )
            );

            outputDebugMessage(
                String.format(
                    "is character at index <%d> valid? <%s>\n",
                    i, isOctalDigitValid(octalString.charAt(i))
                )
            );

            if (!isOctalDigitValid(octalString.charAt(i))) {
                octalNumberValid = false;
                break;
            };
        }

        return octalNumberValid;
    }

    public static void main(String[] args) {
        java.util.Scanner in = new java.util.Scanner(System.in);

        System.out.print("Enter an valid octal number (no more than 4 digits): ");

        String octalNumber = "";

        if (in.hasNextLine()) {
            octalNumber = in.nextLine();
        }

        System.out.printf("Is user's input <%s> valid as required? <%s>\n", octalNumber, isOctalNumberIsValid(octalNumber));
    }
}

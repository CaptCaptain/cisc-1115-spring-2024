import java.util.Scanner;

public class DecimalToOctal {
    public static void outputDebugMessage(String msg) {
        System.out.printf("DEBUG: " + msg);
    }

    public static int getInput(Scanner scanner) {
        System.out.print("Enter a valid decimal number: ");

        int input = scanner.nextInt();

        outputDebugMessage(
            String.format("the user input is <%s>" + "\n", input)
        );

        return input;
    }

    public static int getOctalFromDigit(int n) {
        outputDebugMessage(
            String.format("the remainder of <%d/%d> is <%d>" + "\n", n, 8, n % 8)
        );

        n = n % 8;

        return n;
    }

    public static String getReversedString(String s) {
        String reversedString = "";

        for (int i = s.length(); i > 0; i--) {
            reversedString += s.charAt(i - 1);
        }

        return reversedString;
    }

    public static String getOctalFromDecimal(int decimal) {
        String octal = "";

        while (decimal >= 0) {
            octal += getOctalFromDigit(decimal);

            outputDebugMessage(
                String.format("the quotient of <%d/%d> is <%d>" + "\n", decimal, 8, decimal / 8)
            );

            decimal /= 8;

            outputDebugMessage(
                String.format("the octal number is now <%s>" + "\n", getReversedString(octal))
            );

            if (decimal == 0) {
                break;
            }
        }

        return getReversedString(octal);
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        int inputDecimal = getInput(in);

        System.out.printf("Decimal number %d is octal number %s" + "\n", inputDecimal, getOctalFromDecimal(inputDecimal));
    }
}

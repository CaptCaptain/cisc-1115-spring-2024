import java.util.*;

public class RobustNumberConverter {
    public static Scanner in = new Scanner(System.in);

    public static void outputDebugMessage(String msg) {
        // System.out.printf("DEBUG: " + msg);
    }

    public static Boolean isDecimalDigitValid(char c) {
        Boolean isDecimalDigit = c >= '0' && c <= '9';

        return isDecimalDigit;
    }

    public static Boolean isDecimalNumberValid(String decimalString) {
        Boolean decimalNumberValid = true;

        outputDebugMessage(
            String.format(
                "for a valid octal number, the user entered <%s>\n",
                decimalString
            )
        );

        decimalString = decimalString.trim();

        if (decimalString.length() == 0) {
            return false;
        }

        if (decimalString.length() > 4) {
            return false;
        }

        for (int i = 0; i < decimalString.length(); i++) {
            if (!isDecimalDigitValid(decimalString.charAt(i))) {
                decimalNumberValid = false;
                break;
            };
        }

        return decimalNumberValid;
    }

    public static Boolean isOctalDigitValid(char c) {
        Boolean isOctalDigit = c >= '0' && c <= '7';

        return isOctalDigit;
    }

    public static Boolean isOctalNumberValid(String octalString) {
        Boolean octalNumberValid = true;

        outputDebugMessage(
            String.format(
                "for a valid octal number, the user entered <%s>\n",
                octalString
            )
        );

        octalString = octalString.trim();

        if (octalString.length() == 0) {
            return false;
        }

        if (octalString.length() > 4) {
            return false;
        }

        for (int i = 0; i < octalString.length(); i++) {
            if (!isOctalDigitValid(octalString.charAt(i))) {
                octalNumberValid = false;
                break;
            };
        }

        return octalNumberValid;
    }

    public static int getOctalFromChar(char value, int index, int length) {
        int n = Integer.parseInt(Character.toString(value));
        n = (int) (n * Math.pow(8, index));

        return n;
    }

    public static int getOctalFromString(String string) {
        int n = 0;

        for (int i = string.length(); i > 0; i--) {
            char c = string.charAt(i - 1);
            
            n += getOctalFromChar(c, string.length() - i, string.length());
        }

        return n;
    }

    public static String getOctalNumber() {
        String inputString = "";
        Boolean inputIsValid = false;

        while (!inputIsValid) {
            System.out.print("Enter an valid octal number (no more than 4 digits): ");

            inputString = in.nextLine();

            inputIsValid = isOctalNumberValid(inputString);

            outputDebugMessage(
                String.format("is the input a valid actal number? <%s>\n", inputIsValid)
            );
        }

        return inputString;
    }

    public static int getInputDecimal() {
        String inputString = "";
        Boolean inputIsValid = false;

        while (!inputIsValid) {
            System.out.print("Enter an valid decimal number (no more than 4 digits): ");

            inputString = in.nextLine();

            inputIsValid = isDecimalNumberValid(inputString);

            outputDebugMessage(
                String.format("is the input a valid decimal number? <%s>\n", inputIsValid)
            );
        }

        int input = Integer.parseInt(inputString);

        return input;
    }

    public static int getOctalFromDigit(int n) {
        n = n % 8;

        return n;
    }

    public static String getReversedString(String s) {
        String reversedString = "";

        for (int i = s.length(); i > 0; i--) {
            reversedString += s.charAt(i - 1);
        }

        return reversedString;
    }

    public static String getOctalFromDecimal(int decimal) {
        String octal = "";

        while (decimal >= 0) {
            octal += getOctalFromDigit(decimal);

            decimal /= 8;

            if (decimal == 0) {
                break;
            }
        }

        return getReversedString(octal);
    }

    public static void ConvertOctalNumberToDecimal() {
        String inputString = getOctalNumber();

        System.out.printf("Octal number %s is decimal number %s." + "\n", inputString, getOctalFromString(inputString));
    }

    public static void ConvertDecimalNumberToOctal() {
        int inputDecimal = getInputDecimal();

        System.out.printf("Decimal number %d is octal number %s." + "\n", inputDecimal, getOctalFromDecimal(inputDecimal));
    }

    public static int getMenuChoice() {
        System.out.printf(
            "%s\n%s\n%s\n%s",
            "1. Convert octal number to decimal",
            "2. Decimal decimal number to octal",
            "0. Exit",
            "Enter option (0, 1, or 2) and hit enter: "
        );

        String choice = in.nextLine();
        int choiceNumber = -1;

        if (choice.equals("0")) {
            choiceNumber = 0;
        } else if (choice.equals("1")) {
            choiceNumber = 1;
        } else if (choice.equals("2")) {
            choiceNumber = 2;
        }

        outputDebugMessage(
            String.format(
                "user's input for menu option is <%s>\n",
                choice
            )
        );

        return choiceNumber;
    }

    public static void main(String[] args) {
        int menuChoice;
        do {
            menuChoice = getMenuChoice();

            switch (menuChoice) {
                case 0:
                    System.out.println("Exit the program");
                    break;
                case 1:
                    outputDebugMessage(
                        String.format(
                            "user's option is <%s>\n",
                            menuChoice
                        )
                    );

                    ConvertOctalNumberToDecimal();
                    break;
                case 2:
                    outputDebugMessage(
                        String.format(
                            "user's option is <%s>\n",
                            menuChoice
                        )
                    );

                    ConvertDecimalNumberToOctal();

                    break;
            }
        } while (menuChoice != 0);
    }
}

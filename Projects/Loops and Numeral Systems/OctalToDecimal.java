import java.util.*;

public class OctalToDecimal {
    public static void outputDebugMessage(String msg) {
        System.out.printf("DEBUG: " + msg);
    }

    public static int getOctalFromChar(char value, int index, int length) {
        int n = Integer.parseInt(Character.toString(value));
        n = (int) (n * Math.pow(8, index));

        outputDebugMessage(
            String.format("the value of the power term at index %d is %d" + "\n", length - index - 1, n)
        );

        return n;
    }

    public static int getOctalFromString(String string) {
        int n = 0;

        for (int i = string.length(); i > 0; i--) {
            char c = string.charAt(i - 1);

            outputDebugMessage(
                String.format("the character at index %s is %s" + "\n", i - 1, c)
            );
            
            n += getOctalFromChar(c, string.length() - i, string.length());

            outputDebugMessage(
                String.format("the accumulated decimal number is now %d" + "\n", n)
            );
        }

        return n;
    }

    public static String getInputString(Scanner scanner) {
        System.out.print("Enter a valid octal number: ");

        String inputString = scanner.nextLine();

        outputDebugMessage(
            String.format("the trimmed input is <%s>" + "\n", inputString)
        );

        return inputString.trim();
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        String inputString = getInputString(in);

        System.out.printf("Octal number %s is decimal number %s" + "\n", inputString, getOctalFromString(inputString));
    }
}

import java.util.*;

public class NumberConverter {
    public static Scanner in = new Scanner(System.in);

    public static void outputDebugMessage(String msg) {
        System.out.printf("DEBUG: " + msg);
    }

    public static int getOctalFromChar(char value, int index, int length) {
        int n = Integer.parseInt(Character.toString(value));
        n = (int) (n * Math.pow(8, index));

        outputDebugMessage(
            String.format("the value of the power term at index %d is %d" + "\n", length - index - 1, n)
        );

        return n;
    }

    public static int getOctalFromString(String string) {
        int n = 0;

        for (int i = string.length(); i > 0; i--) {
            char c = string.charAt(i - 1);

            outputDebugMessage(
                String.format("the character at index %s is %s" + "\n", i - 1, c)
            );
            
            n += getOctalFromChar(c, string.length() - i, string.length());

            outputDebugMessage(
                String.format("the accumulated decimal number is now %d" + "\n", n)
            );
        }

        return n;
    }

    public static String getOctalNumber() {
        System.out.print("Enter an valid octal number (no more than 4 digits): ");

        if (in.hasNext()) {
            String inputString = in.next();

            return inputString.trim();
        }

        return "";
    }

    public static int getInputDecimal() {
        System.out.print("Enter an valid decimal number (no more than 4 digits): ");

        int input = in.nextInt();

        outputDebugMessage(
            String.format("user entered decimal number <%s>\n", input)
        );

        return input;
    }

    public static int getOctalFromDigit(int n) {
        outputDebugMessage(
            String.format("the remainder of <%d/%d> is <%d>\n", n, 8, n % 8)
        );

        n = n % 8;

        return n;
    }

    public static String getReversedString(String s) {
        String reversedString = "";

        for (int i = s.length(); i > 0; i--) {
            reversedString += s.charAt(i - 1);
        }

        return reversedString;
    }

    public static String getOctalFromDecimal(int decimal) {
        String octal = "";

        while (decimal >= 0) {
            octal += getOctalFromDigit(decimal);

            outputDebugMessage(
                String.format("the quotient of <%d/%d> is <%d>" + "\n", decimal, 8, decimal / 8)
            );

            decimal /= 8;

            outputDebugMessage(
                String.format("the octal number is now <%s>" + "\n", getReversedString(octal))
            );

            if (decimal == 0) {
                break;
            }
        }

        return getReversedString(octal);
    }

    public static void ConvertOctalNumberToDecimal() {
        String inputString = getOctalNumber();

        outputDebugMessage(
            String.format("user entered octal number <%s>" + "\n", inputString)
        );

        System.out.printf("Octal number %s is decimal number %s." + "\n", inputString, getOctalFromString(inputString));
    }

    public static void ConvertDecimalNumberToOctal() {
        int inputDecimal = getInputDecimal();

        System.out.printf("Decimal number %d is octal number %s." + "\n", inputDecimal, getOctalFromDecimal(inputDecimal));
    }

    public static int getMenuChoice() {
        System.out.printf(
            "%s\n%s\n%s\n%s",
            "1. Convert octal number to decimal",
            "2. Decimal decimal number to octal",
            "0. Exit",
            "Enter option (0, 1, or 2) and hit enter: "
        );

        int choice = in.nextInt();

        outputDebugMessage(
            String.format(
                "user's input for the menu option is <%s>\n",
                choice
            )
        );

        return choice;
    }

    public static void main(String[] args) {
        int menuChoice;
        do {
            menuChoice = getMenuChoice();

            switch (menuChoice) {
                case 0:
                    System.out.println("Exit the program");
                    break;
                case 1:
                    outputDebugMessage(
                        String.format(
                            "user's option is <%s>\n",
                            menuChoice
                        )
                    );

                    ConvertOctalNumberToDecimal();
                    break;
                case 2:
                    outputDebugMessage(
                        String.format(
                            "user's option is <%s>\n",
                            menuChoice
                        )
                    );

                    ConvertDecimalNumberToOctal();

                    break;
            }
        } while (menuChoice != 0);
    }
}

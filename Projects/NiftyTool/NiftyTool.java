import java.util.Arrays;

public class NiftyTool {
    public static void main(String[] args) {
        if (args.length == 0) {
            help();
            return;
        }

        String command = args[0];
        String[] commandArgs = getCommandArgs(args);

        switch (command) {
            case "Sum":
                argSum(commandArgs);
                break;
            case "MinValue":
                argMinValue(commandArgs);
                break;
            case "MaxElement":
                argMaxElement(commandArgs);
                break;
            case "Average":
                argAverage(commandArgs);
                break;
            case "CountLowers":
                argCountLowers(commandArgs);
                break;
            case "LongestRun":
                argLongestRun(commandArgs);
                break;
            case "Reverse":
                argReverse(commandArgs);
                break;
            default:
                if (command.indexOf("Swap") == 0) {
                    String[] commandParameters = getCommandParameters(command, 2);

                    argSwap(commandParameters, commandArgs);
                } else if (command.indexOf("Shift") == 0) {
                    String[] commandParameters = getCommandParameters(command, 2);

                    argShift(commandParameters, commandArgs);
                } else {
                    System.out.printf("Command \"%s\" not yet supported\n", command);
                    help();
                }
                break;
        }
    }

    public static String[] getCommandParameters(String command, int parameterCount) {
        String[] parameterList = new String[parameterCount];

        int parameterIndex = 0;
        String parameter = "";

        for (int i = command.indexOf('(') + 1; i < command.indexOf(')'); i++) {
            if (command.charAt(i) == ',') {
                parameterList[parameterIndex] = parameter;
                parameter = "";
                parameterIndex++;
            } else {
                parameter += command.charAt(i);
            }
        }

        parameterList[parameterIndex] = parameter;

        return parameterList;
    }

    public static String[] getCommandArgs(String[] args) {
        String[] commandArgs = new String[Math.max(0, args.length - 1)];

        for (int i = 1; i < args.length; i++) {
            commandArgs[i - 1] = args[i];
        }

        return commandArgs;
    }

    public static String[] getCommandArgs(String[] args, int start) {
        String[] commandArgs = new String[Math.max(0, args.length - start)];

        for (int i = start; i < args.length; i++) {
            commandArgs[i - start] = args[i];
        }

        return commandArgs;
    }

    public static double[] getDoubleList(String[] commandArgs) {
        double[] numList = new double[commandArgs.length];

        for (int i = 0; i < commandArgs.length; i++) {
            numList[i] = Double.parseDouble(commandArgs[i]);
        }

        return numList;
    }

    public static int[] getIntList(String[] commandArgs) {
        int[] numList = new int[commandArgs.length];

        for (int i = 0; i < commandArgs.length; i++) {
            numList[i] = Integer.parseInt(commandArgs[i]);
        }

        return numList;
    }

    public static void argSwap(String[] commandParameters, String[] commandArgs) {
        String[] swappedArray = commandArgs.clone();

        swapElements(swappedArray, Integer.parseInt(commandParameters[0]), Integer.parseInt(commandParameters[1]));
        
        System.out.printf("Swap(%s,%s,%s) -> %s\n", Arrays.toString(commandArgs).replace(" ", ""), commandParameters[0], commandParameters[1], Arrays.toString(swappedArray).replace(" ", ""));
    }

    public static void swapElements(String[] arr, int i, int j) {
        String temp = arr[i];
        
        arr[i] = arr[j];
        arr[j] = temp;
    }

    public static void argShift(String[] commandParameters, String[] commandArgs) {
        String[] shiftedArray = commandArgs.clone();

        circularShift(shiftedArray, Integer.parseInt(commandParameters[0]), commandParameters[1].charAt(0));
        
        System.out.printf("Shift(%s,%s,%s) -> %s\n", Arrays.toString(commandArgs).replace(" ", ""), commandParameters[0], commandParameters[1], Arrays.toString(shiftedArray).replace(" ", ""));
    }

    public static void circularShift(String[] arr, int position, char direction) {
        int shift = direction == 'L' ? 1 : (direction == 'R' ? -1 : 0);
        
        String[] tempList = arr.clone();

        for (int i = 0; i < arr.length; i++) {
            int index = (i + Math.min(position, arr.length) * shift);

            index = index < 0 ? arr.length + index :
                    (
                        index >= arr.length ? index - arr.length :
                        index
                    );

            arr[i] = tempList[index];
        }
    }

    public static void argReverse(String[] commandArgs) {
        String[] reversedArray = getReversedArray(commandArgs);
        
        System.out.printf("Reverse(%s) -> %s\n", Arrays.toString(commandArgs).replace(" ", ""), Arrays.toString(reversedArray).replace(" ", ""));
    }

    public static String[] getReversedArray(String[] commandArgs) {
        String[] reversedList = commandArgs.clone();
        String[] tempList = commandArgs.clone();
        
        for (int i = 0; i < tempList.length; i++) {
            reversedList[tempList.length - i - 1] = tempList[i];
        }

        return reversedList;
    }

    public static void argLongestRun(String[] commandArgs) {
        int[] numList = getIntList(commandArgs);

        int[] longestRun = getLongestRun(numList);

        System.out.printf("LongestRun(%s) -> %s\n", Arrays.toString(commandArgs).replace(" ", ""), Arrays.toString(longestRun).replace(" ", ""));
    }

    public static int[] getLongestRun(int[] intList) {
        int[] longestRun = new int[3];

        final int startIndex = 0;
        final int length = 1;
        final int value = 2;
        
        int previous = intList[0];
        int run = 1;
        int index = 0;
        int bestValue = previous;
        int bestRun = run;
        int bestIndex = 0;

        for (int i = 1; i < intList.length; i++) {
            int current = intList[i];

            if (current == previous) {
                run++;
            } else {
                previous = current;
                run = 1;
                index = i;
            }

            if (run > bestRun) {
                bestRun = run;
                bestValue = current;
                bestIndex = index;
            }
        }

        longestRun[startIndex] = bestIndex;
        longestRun[length] = bestRun;
        longestRun[value] = bestValue;

        return longestRun;
    }

    public static void argCountLowers(String[] commandArgs) {
        int[] lowerCount = getLowerCount(commandArgs);

        String argString = getCharArrayString(commandArgs);
        String outputString = getIntDictionaryString(lowerCount);

        System.out.printf("CountLowers(%s) -> %s\n", argString, outputString);
    }

    public static String getCharArrayString(String[] commandArgs) {
        String string = "";

        string += "[";

        for (int i = 0; i < commandArgs.length; i++) {

            if (i == 0) string += String.format("'%s", commandArgs[i]);
            else string += String.format("'%s'", commandArgs[i]);
            
            if (i < commandArgs.length - 1) string += ",";
        }

        string += "]";

        return string;
    }

    public static String getIntDictionaryString(int[] lowerCount) {
        String string = "";

        string += "{";

        for (int i = 0; i < lowerCount.length; i++) {
            char character = (char) ('a' + i);

            string += String.format("'%s':%s", character, lowerCount[i]);
            
            if (i < lowerCount.length - 1) {
                string += ",";
            }
        }

        string += "}";

        return string;
    }

    public static int[] getLowerCount(String[] commandArgs) {
        int[] lowerCount = new int['z' - 'a' + 1];

        for (int i = 0; i < commandArgs.length; i++) {
            if (Character.isLowerCase(commandArgs[i].charAt(0))) {
                char character = commandArgs[i].charAt(0);
                
                lowerCount[('z' - 'a') - ('z' - character)]++;
            }
        }

        return lowerCount;
    }

    public static void argAverage(String[] commandArgs) {
        double[] numList = getDoubleList(commandArgs);

        double average = getAverage(numList);

        System.out.printf("Average(%s) -> %.6f\n", Arrays.toString(numList).replace(" ", ""), average);
    }

    private static double getAverage(double[] numList) {
        double sum = getSum(numList);

        double average = sum / numList.length;

        return average;
    }

    public static void argMaxElement(String[] commandArgs) {
        double[] numList = getDoubleList(commandArgs);

        int min = getMaxElement(numList);

        System.out.printf("MaxElement(%s) -> %d\n", Arrays.toString(numList).replace(" ", ""), min);
    }

    public static int getMaxElement(double[] numList) {
        int maxIndex = 0;

        for (int i = 0; i < numList.length; i++) {
            if (numList[i] > numList[maxIndex]) {
                maxIndex = i;
            }
        }
        
        return maxIndex;
    }

    public static void argMinValue(String[] commandArgs) {
        double[] numList = getDoubleList(commandArgs);

        double min = getMinValue(numList);

        System.out.printf("MinValue(%s) -> %.6f\n", Arrays.toString(numList).replace(" ", ""), min);
    }

    public static double getMinValue(double[] numList) {
        double min = numList[0];

        for (int i = 0; i < numList.length; i++) {
            if (min > numList[i]) {
                min = numList[i];
            }
        }
        
        return min;
    }

    public static void argSum(String[] commandArgs) {
        double[] numList = getDoubleList(commandArgs);

        double sum = getSum(numList);

        System.out.printf("Sum(%s) -> %.0f\n", Arrays.toString(commandArgs).replace(" ", ""), sum);
    }

    public static double getSum(double[] numList) {
        double sum = 0;

        for (int i = 0; i < numList.length; i++) {
            sum += numList[i];
        }

        return sum;
    }

    public static void help() {
        System.out.println("Usage: NiftyTool command ...");
    }

    public static void parseArgsToDoubleArray() {}
    public static void getSmallestValue() {}
    public static void reverseArray() {}
    public static void parseArgsToIntArray() {}
    public static void parseArgsToCharArray() {}
    public static void letterCountsToString() {}
    public static void cloneArray() {}
    public static void getLargestElement() {}
    public static void arrayToString() {}
    public static void parseArgsToStringArray() {}
    public static void countLowerCaseLetters() {}
    public static void findLongestRun() {}
}
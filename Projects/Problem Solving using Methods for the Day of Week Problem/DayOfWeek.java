import java.util.Scanner;

public class DayOfWeek {
    public static final int INPUT_DATE = 0;
    public static final int INPUT_WEEK = 1;

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        String today = readFromUser(in, "Enter today's date (MM/DD/CCYY): ", INPUT_DATE);

        if (!isValidInput(today, INPUT_DATE)) {
            return;
        }

        String todayWeek = readFromUser(in, "Enter today's day of week (0 - 6): ", INPUT_WEEK);

        if (!isValidInput(todayWeek, INPUT_WEEK)) {
            return;
        }

        printDayOfWeek(in, today, todayWeek);
    }

    public static String readFromUser(Scanner in, String prompt, int inputType) {
        while (true) {
            System.out.print(prompt);

            String userInput = in.next();

            if (inputType == INPUT_DATE && userInput.equals("01/01/0000")) {
                return "";
            }

            if (isValidInput(userInput, inputType)) {
                return userInput;
            }
        }
    }

    public static void printDayOfWeek(Scanner scanner, String today, String todayDayOfWeek) {
        while (true) {
            String date = readFromUser(scanner, "Enter a date (MM/DD/CCYY): ", INPUT_DATE);

            if (date == "") {
                return;
            }

            int month = getDateField(date, 'M');
            int day = getDateField(date, 'D');
            int year = getDateField(date, 'Y');

            int dayOfWeek = computeDayOfWeek(year, month, day);

            System.out.printf("%s %d, %d is %s\n", getMonthName(month), day, year, getDayName(dayOfWeek));
        }
    }

    public static Boolean isValidDigit(String input) {
        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);

            // Verify if character is numeric or /
            if (!Character.isDigit(c)) return false;
        }

        return true;
    }

    public static Boolean isValidDate(String input) {
        // Verify length
        if (input.length() != 10) {
            return false;
        }

        // Verify number
        String monthString = input.substring(0, 2);
        String dayString = input.substring(3, 5);
        String yearString = input.substring(6, 10);

        if (!(isValidDigit(monthString) && isValidDigit(dayString) && isValidDigit(yearString))) {
            return false;
        }

        // Verify date
        int month = getDateField(input, 'M');
        int day = getDateField(input, 'D');
        int year = getDateField(input, 'Y');

        Boolean monthWithinRange = month >= 0 && month <= 12;
        if (!monthWithinRange) {
            return false;
        }

        Boolean dayWithinRange = day >= 0 && day <= getNumberOfDaysInMonth(year, month);
        if (!dayWithinRange) {
            return false;
        }

        Boolean yearWithinRange = year > 0;
        if(!yearWithinRange) {
            return false;
        }

        return true;
    }

    public static Boolean isValidWeek(String input) {
        // Verify length
        if (input.length() != 1) {
            return false;
        }

        // Verify digit
        if (!isValidDigit(input)) {
            return false;
        }

        // Verify range
        int week = Integer.parseInt(input);

        Boolean weekWithinRange = week >= 0 && week <= 6;

        if (!weekWithinRange) {
            return false;
        }

        return true;
    }

    public static boolean isValidInput(String input, int inputType) {
        if (input.length() == 0) {
            return false;
        }

        switch (inputType) {
            case INPUT_DATE:
                return isValidDate(input);
            case INPUT_WEEK:
                return isValidWeek(input);
        }

        return false;
    }

    public static int getNumberOfDaysInMonth(int year, int month) {
        int days = 0;

        if (month == 1 ||
            month == 3 ||
            month == 5 ||
            month == 7 ||
            month == 8 ||
            month == 10 ||
            month == 12) {
            days = 31;
        } else if (
            month == 4 ||
            month == 6 ||
            month == 9 ||
            month == 11) {
            days = 30;
        } else if (month == 2 && isLeapYear(year)) {
            days = 29;
        } else if (month == 2) {
            days = 28;
        }

        return days;
    }

    public static boolean isLeapYear(int year) {
        if (year % 4 != 0) {
            return false;
        } else if (year % 400 == 0) {
            return true;
        } else if (year % 100 == 0) {
            return false;
        } else {
            return true;
        }
    }
    
    public static int getTotalNumberOfDays(int year, int month, int day, int refYear, int refMonth, int refDay) {
        if (refYear == year && refMonth == month) {
            int days = day - refDay;
            return days;
        } else {
            int remainingDaysInRefMonth = getNumberOfDaysInMonth(refYear, refMonth) - refDay;
            int daysInGivenMonth = day;
            int daysOfWholeMonths = 0;

            int m = refMonth;
            int y = refYear;

            while (true) {
                if (m == month && y == year) break;
                m++;
                y = (m - 1) / 12 + y;
                m = (m - 1) % 12 + 1;
                if (m == month && y == year) break;
                daysOfWholeMonths += getNumberOfDaysInMonth(y, m);
            }

            int daysInBetween = remainingDaysInRefMonth + daysOfWholeMonths + daysInGivenMonth;

            return daysInBetween;
        }
    }

    public static int getTotalNumberOfDaysAhead(int year, int month, int day, int refYear, int refMonth, int refDay) {
        return getTotalNumberOfDays(year, month, day, refYear, refMonth, refDay);
    }

    public static int getTotalNumberOfDaysBehind(int year, int month, int day, int refYear, int refMonth, int refDay) {
        return getTotalNumberOfDays(refYear, refMonth, refDay, year, month, day);
    }

    public static int computeDayOfWeek(int year, int month, int day) {
        if (month < 3) {
            month += 12;
            year--;
        }

        return (day + (int)((month+1) * 2.6) + year + (int)(year / 4) + 6 * (int)(year / 100) + (int)(year / 400) + 6) % 7;
    }

    public static int getDateField(String date, char type) {
        String monthString = date.substring(0, 2);
        String dayString = date.substring(3, 5);
        String yearString = date.substring(6, 10);

        int month = Integer.parseInt(monthString);
        int day = Integer.parseInt(dayString);
        int year = Integer.parseInt(yearString);
                
        switch (type) {
            case (char) 'Y':
                return year;
            case (char) 'M':
                return month;
            case (char) 'D':
                return day;
        }

        return 0;
    }

    public static String getMonthName(int month) {
        String[] monthNames = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
        
        return monthNames[month - 1];
    }

    public static String getDayName(int day) {
        String[] dayNames = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};

        return dayNames[day];
    }
}

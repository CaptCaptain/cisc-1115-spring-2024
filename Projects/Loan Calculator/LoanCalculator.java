import java.util.Scanner;

public class LoanCalculator {
    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);

        /* Declare variables and get user input */

        System.out.println("Amortized Loan Calculator v1.0");

        System.out.print("Enter the principle of the loan (in whole dollars, e.g., 1000): ");

        int Principle = stdin.nextInt();

        System.out.print("Enter the term of the loan (in years, e.g., 5): ");

        int Term = stdin.nextInt();

        System.out.print("Enter the annual interest rate (APR, e.g., 5.6): ");

        double APR = stdin.nextDouble();

        /* Perform calculation */

        final double P = Principle;
        final double r = (APR / 100.);
        final double n = 12;
        final double t = Term;
        final double A = (P * r/n) / (1 - Math.pow(1+r/n, -n * t));

        double MonthlyPayment = A;
        MonthlyPayment = (int) (MonthlyPayment * 100.) / 100.;

        final double S = n * t * A;

        double TotalPayment = S;
        TotalPayment = (int) (TotalPayment * 100.) / 100.;

        final double I = S - P;

        double TotalInterest = I;
        TotalInterest = (int) (TotalInterest * 100.) / 100.;

        System.out.println("");

        /* Output result */

        String Whitespaces = "                                                                      ";
        
        // Using the substring of a string of whitespaces instead of manually typing whitespaces and formatting strings with String.format()
        System.out.println(String.format("%sSummary of the Loan", Whitespaces.substring(0, 10)));
        System.out.println(String.format("%sPrinciple: $%s", Whitespaces.substring(0, 10), Principle));
        System.out.println(String.format("%sAPR: %s%%", Whitespaces.substring(0, 16), APR));
        System.out.println(String.format("%sTerm: %s years", Whitespaces.substring(0, 15), Term));
        System.out.println(String.format("%sMonthly payment: $%s", Whitespaces.substring(0, 4), MonthlyPayment));
        System.out.println(String.format("%sTotal payment: $%s", Whitespaces.substring(0, 6), TotalPayment));
        System.out.println(String.format("%sTotal interest: $%s", Whitespaces.substring(0, 5), TotalInterest));
    }
}

import java.util.Scanner;

public class CheckingSign {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.print("Enter floating point value: ");

        String inputNumber = in.nextLine();

        System.out.println(String.format(
            "DEBUG: the user input is <%s>",
            inputNumber
        ));

        inputNumber = inputNumber.trim();

        System.out.println(String.format(
            "DEBUG: the trimed input is <%s>",
            inputNumber
        ));

        if (inputNumber.contains("+")) {
            System.out.println(String.format("The sign of the input is %s", 1));
        } else if (inputNumber.contains("-")) {
            System.out.println(String.format("The sign of the input is %s", -1));
        } else if (inputNumber.equals("")) {
            System.out.println("Encountered blank input.");
        }
    }
}

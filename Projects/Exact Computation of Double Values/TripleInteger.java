import java.util.Scanner;

public class TripleInteger {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.print("Enter an integer: ");

        int input = in.nextInt();

        int tripledNumber = input * 3;

        String inputString = Integer.toString(input);
        String tripledNumberString = Integer.toString(tripledNumber);

        int carryOver = tripledNumber / 1000;

        if (carryOver > 0) {
            System.out.println("3 * " + input + " is " + tripledNumber + " with a carrying over of " + carryOver);
            return;
        }

        System.out.println("3 * " + input + " is " + tripledNumber + " without a carrying over");
    }
}

import java.util.Scanner;

public class ExactTripleFloat {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        while (true) {
            System.out.print("Enter a float point value: ");

            String input = in.nextLine();
            String inputNumber = input.trim();
            int numberSign = 1;

            // System.out.println("DEBUG: the user input is " + "<" + input + ">");

            if (input.toLowerCase().equals("exit")) {
                break;
            }

            // System.out.println("DEBUG: the trimmed input is " + "<" + inputNumber + ">");

            if (inputNumber.equals("")) {
                System.out.println("Encountered blank input.");
                continue;
            }

            if (inputNumber.contains("-")) {
                numberSign = -1;
            }

            String numberSignString = numberSign == 1 ? "+" : "";

            // System.out.println("DEBUG: the sign of the input is " + "<" + numberSign + ">");

            inputNumber = inputNumber.replace("+", "").replace("-", "").replace(" ", "");

            int decimalPointIndex = inputNumber.indexOf(".");

            // System.out.println("DEBUG: the index of the decimal point is " + "<" + decimalPointIndex + ">");

            String wholeNumber = "0";

            if (decimalPointIndex != -1) {
                wholeNumber = inputNumber.substring(0, Math.min(decimalPointIndex, inputNumber.length()));
            } else {
                wholeNumber = inputNumber.substring(0, inputNumber.length());
            }

            wholeNumber = wholeNumber.equals("") ? "0" : wholeNumber;

            // System.out.println("DEBUG: the whole number part of the input is " + "<" + wholeNumber + ">");

            /*
            for (int i = 0; i < wholeNumber.length(); i++) {
                System.out.println("DEBUG: char at index " + "<" + i + ">" + " of the whole number part = " + "<" + wholeNumber.charAt(i) + ">");
            }
            */

            Boolean wholeNumberValid = true;

            for (int i = 0; i < wholeNumber.length(); i++) {
                if (!Character.isDigit(wholeNumber.charAt(i))) {
                    wholeNumberValid = false;
                    break;
                }
            }

            // System.out.println("DEBUG: the validity of the whole number part is " + "<" + wholeNumberValid + ">");

            String fractionNumber = "0";

            if (decimalPointIndex != -1) {
                fractionNumber = inputNumber.substring(Math.min(decimalPointIndex + 1, inputNumber.length()));
            }

            fractionNumber = fractionNumber.length() == 0 ? "0" : fractionNumber;

            // System.out.println("DEBUG: the fraction part of the input is " + "<" + fractionNumber + ">");

            /*
            for (int i = 0; i < fractionNumber.length(); i++) {
                System.out.println("DEBUG: char at index " + "<" + i + ">" + " of the fraction part = " + "<" + fractionNumber.charAt(i) + ">");
            }
            */

            Boolean fractionNumberValid = true;

            for (int i = 0; i < fractionNumber.length(); i++) {
                if (!Character.isDigit(fractionNumber.charAt(i))) {
                    fractionNumberValid = false;
                    break;
                }
            }

            // System.out.println("DEBUG: the validity of the fraction part is " + "<" + fractionNumberValid + ">");

            if (wholeNumberValid && fractionNumberValid) {
                // System.out.println("DEBUG: the input " + "<" + input + ">" + " is a " + "valid" + " double value of " + "<" + numberSign + ">" + " " + "<" + wholeNumber + ">" + "." + "<" + fractionNumber + ">");

                int tripledWholeNumber = Integer.parseInt(wholeNumber) * 3;

                int tripledFractionalNumber = Integer.parseInt(fractionNumber) * 3;

                int carryOver = tripledFractionalNumber / 1000;

                int carryOverRemainder = tripledFractionalNumber % 1000;

                String tripledFractionalNumberLeadingZeroes = Double.toString(Double.parseDouble("." + fractionNumber) * 3).substring(2);

                double tripleNumber = Math.round(Double.parseDouble(wholeNumber + "." + fractionNumber) * 3 * 100000) / 100000.0;

                double result = numberSign * tripleNumber;

                if (carryOver > 0) {
                    // System.out.println("DEBUG: 3 * " + "<" + fractionNumber + ">" + " is " + carryOverRemainder + " with a carrying over of " + carryOver);

                    if (wholeNumber != "0" && fractionNumber == "0") {
                        // System.out.println("DEBUG: triple fraction minus carry over is " + "<" + carryOverRemainder + ">");

                        // System.out.println("DEBUG: triple fraction carry over is " + "<" + carryOver + ">");

                        continue;
                    }

                    // System.out.println("DEBUG: triple whole number plus triple fraction carry over is " + "<" + ((int) tripleNumber) + ">");

                    System.out.println("The result is " + (numberSignString + result));

                    continue;
                }

                // System.out.println("DEBUG: 3 * " + fractionNumber + " is " + tripledFractionalNumberLeadingZeroes + " without a carrying over");

                // System.out.println("DEBUG: triple whole number plus triple fraction carry over is " + "<" + ((int) tripleNumber) + ">");

                System.out.println("The result is " + (numberSignString + result));

                continue;
            }

            System.out.println("The input " + "<" + input + ">" + " is an " + "invalid" + " double value");
        }
    }
}
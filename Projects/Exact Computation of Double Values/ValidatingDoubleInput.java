import java.util.Scanner;

public class ValidatingDoubleInput {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.print("Enter a float point value: ");

        String input = in.nextLine();
        String inputNumber = input.trim();

        System.out.println("DEBUG: the user input is " + "<" + input + ">");

        System.out.println("DEBUG: the trimmed input is " + "<" + inputNumber + ">");

        if (inputNumber.equals("")) {
            System.out.println("Encountered blank input.");
            return;
        }

        int decimalPointIndex = inputNumber.indexOf(".");

        System.out.println("DEBUG: the index of the decimal point is " + "<" + decimalPointIndex + ">");

        String wholeNumber = "0";

        if (decimalPointIndex != -1) {
            wholeNumber = inputNumber.substring(0, Math.min(decimalPointIndex, inputNumber.length()));
        } else {
            wholeNumber = inputNumber.substring(0, inputNumber.length());
        }

        wholeNumber = wholeNumber.equals("") ? "0" : wholeNumber;

        System.out.println("DEBUG: the whole number part of the input is " + "<" + wholeNumber + ">");

        for (int i = 0; i < wholeNumber.length(); i++) {
            System.out.println("DEBUG: char at index " + "<" + i + ">" + " of the whole number part = " + "<" + wholeNumber.charAt(i) + ">");
        }

        Boolean wholeNumberValid = true;

        for (int i = 0; i < wholeNumber.length(); i++) {
            if (!Character.isDigit(wholeNumber.charAt(i))) {
                wholeNumberValid = false;
                break;
            }
        }

        System.out.println("DEBUG: the validity of the whole number part is " + "<" + wholeNumberValid + ">");

        String fractionNumber = "0";

        if (decimalPointIndex != -1) {
            fractionNumber = inputNumber.substring(Math.min(decimalPointIndex + 1, inputNumber.length()));
        }

        fractionNumber = fractionNumber.length() == 0 ? "0" : fractionNumber;

        System.out.println("DEBUG: the fraction part of the input is " + "<" + fractionNumber + ">");

        for (int i = 0; i < fractionNumber.length(); i++) {
            System.out.println("DEBUG: char at index " + "<" + i + ">" + " of the fraction part = " + "<" + fractionNumber.charAt(i) + ">");
        }

        Boolean fractionNumberValid = true;

        for (int i = 0; i < fractionNumber.length(); i++) {
            if (!Character.isDigit(fractionNumber.charAt(i))) {
                fractionNumberValid = false;
                break;
            }
        }

        System.out.println("DEBUG: the validity of the fraction part is " + "<" + fractionNumberValid + ">");

        if (wholeNumberValid && fractionNumberValid) {
            System.out.println("The input " + "<" + input + ">" + " is a " + "valid" + " double value of " + "<" + wholeNumber + ">" + "." + "<" + fractionNumber + ">");

            return;
        }

        System.out.println("The input " + "<" + input + ">" + " is an " + "invalid" + " double value");
    }
}
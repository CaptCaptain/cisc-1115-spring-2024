import java.util.Scanner;

public class NiceCylinderCalculator {
	public static void main(String[] args) {
		Scanner stdin = new Scanner(System.in);
		
		System.out.print("Enter the radius of the cylinder: ");
		
		double radius = stdin.nextDouble();
		
		System.out.print("Enter the length of the cylinder: ");
		
		double length = stdin.nextDouble();
		
		System.out.println(String.format("The radius and the length of the cylinder are %s and %s.", radius, length));
		
		double area = (2 * Math.PI * radius * radius) + (2 * Math.PI * radius * length);
		
		double volume = Math.PI * radius * radius * length;
		
		area = (int) (area * 1000.) / 1000.;
		volume = (int) (volume * 1000.) / 1000.;
		
		System.out.println(String.format("The surface area of the cylinder is %s.", area));
		System.out.println(String.format("The volume of the cylinder is %s.", volume));
	}
}
import java.util.*;

public class CelsiusToFahrenheit {
    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);

        System.out.println("Please enter a temperature in Celsius:");

        float celsius = stdin.nextFloat();

        float fahrenheit = (9f / 5f) * celsius + 32;

        System.out.println(celsius + " degrees Celsius is " + fahrenheit + " degrees Fahrenheit");
    }
}

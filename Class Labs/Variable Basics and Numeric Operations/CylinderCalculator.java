import java.util.*;

public class CylinderCalculator {
    public static void main(String[] args) {
        // Declare a variable with a new scanner object
        Scanner scanner = new Scanner(System.in);
        
        // Output that we want the user to enter the radius of the cylinder
        System.out.println("Enter the radius of the cylinder:");

        // Declare radius and set it to the next float from the scanner
        float radius = scanner.nextFloat();

        // Output that we want the user to enter the radius of the cylinder
        System.out.println("Enter the length of the cylinder:");

        // Declare length and set it to the next float from the scanner
        float length = scanner.nextFloat();

        // Declare surface_area and set to calculation
        double surface_area = (2 * Math.PI * radius * radius) + (2 * Math.PI * radius * length);
        
        // Declare volume and set to calculation
        double volume = (Math.PI * radius * radius * length);

        // Output results
        System.out.println(String.format("The radius and the length of the cylinder are %s and %s", radius, length));
        System.out.println(String.format("The surface area of the cylinder is %s", surface_area));
        System.out.println(String.format("The volume of the cylinder is %s", volume));
    }
}

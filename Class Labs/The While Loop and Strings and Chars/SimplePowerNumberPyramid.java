public class SimplePowerNumberPyramid {
    public static String WHITESPACE = "                              ";
    public static void main(String[] args) {
        java.util.Scanner in = new java.util.Scanner(System.in);

        System.out.print("Enter the height of number pyramid: ");

        int heightOfPyramid = in.nextInt();

        if (heightOfPyramid > 4) {
            System.out.println(String.format("Height cannot be greater 4, encountered %s.", heightOfPyramid));

            return;
        }

        for (int y = 1; y < heightOfPyramid + 1; y++) {
            String textAtHeight = "";

            textAtHeight += WHITESPACE.substring(0, (heightOfPyramid - y) * 2);

            for (int x = 1; x < y + 1; x++) {
                textAtHeight += (int) Math.pow(2, x - 1) + " ";
            }

            for (int x = y - 1; x > 0; x--) {
                textAtHeight += (int) Math.pow(2, x - 1) + " ";
            }

            System.out.println(textAtHeight);
        }
    }
}

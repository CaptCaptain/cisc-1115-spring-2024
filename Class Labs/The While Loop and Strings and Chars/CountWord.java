public class CountWord {
    public static void main(String[] args) {
        java.util.Scanner in = new java.util.Scanner(System.in);

        System.out.println("Enter a line of text:");

        String lineOfText = in.nextLine();

        System.out.println("Enter a word:");

        String wordToSearch = in.nextLine();

        int lastIndexSearched = 0;
        int numberOfWordsFound = 0;

        while (true) {
            int wordFoundAtIndex = lineOfText.indexOf(wordToSearch, lastIndexSearched);
            Boolean wordFound = wordFoundAtIndex != -1;

            if (!wordFound) {
                break;
            }

            numberOfWordsFound += 1;
            lastIndexSearched = wordFoundAtIndex + wordToSearch.length() - 1;
        }

        System.out.println(String.format("Word <%s> appears %s times in line <%s>.", wordToSearch, numberOfWordsFound, lineOfText));
    }
}

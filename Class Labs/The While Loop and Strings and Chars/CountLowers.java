public class CountLowers {
    public static void main(String[] args) {
        java.util.Scanner in = new java.util.Scanner(System.in);

        System.out.println("Enter a line of text:");

        String lineOfText = in.nextLine();
        int numberOfLowerCaseCharacters = 0;

        for (int i = 0; i < lineOfText.length(); i++) {
            Character currentCharacter = lineOfText.charAt(i);

            if (Character.isLowerCase(currentCharacter)) {
                numberOfLowerCaseCharacters += 1;
            }
        }

        System.out.println(String.format("The number of lower case letters in the input is: %s.", numberOfLowerCaseCharacters));
    }
}
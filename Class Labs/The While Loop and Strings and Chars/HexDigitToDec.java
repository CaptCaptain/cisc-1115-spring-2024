public class HexDigitToDec {
    public static String HEX_CHARS = "0123456789ABCDEF";
    public static void main(String[] args) {
        java.util.Scanner in = new java.util.Scanner(System.in);

        while (true) {
            System.out.print("Enter a hexadecimal digit (0-9, A-F): ");

            String input = in.next().toUpperCase();

            Boolean inputIsEscape = input.toLowerCase().equals("done");
            Boolean inputIsNotOneCharacter = input.length() > 1;
            Boolean inputIsInvalid = HEX_CHARS.indexOf(input.charAt(0)) < 0;
            
            if (inputIsEscape) {
                break;
            }

            if (inputIsNotOneCharacter) {
                System.out.println(String.format("Expected one character per line, but saw %s.", input.length()));

                continue;
            }

            if (inputIsInvalid) {
                System.out.println(String.format("Expected 0 - 9 or A - F, but encountered %s.", input));

                continue;
            }

            int number = HEX_CHARS.indexOf(input.charAt(0)) * (int) Math.pow(16., input.length() - 1);

            System.out.println(String.format("Hexadecimal digit %s is %s in decimal.", input, number));
        }
    }
}

public class GPACalculator {
    public static void main(String[] args) {
        java.util.Scanner Scanner = new java.util.Scanner(System.in);

        double GPA1 = Scanner.nextDouble();
        double Credits1 = Scanner.nextDouble();

        double GPA2 = Scanner.nextDouble();
        double Credits2 = Scanner.nextDouble();

        double GPA = (((GPA1 * Credits1) + (GPA2 * Credits2)) / (Credits1 + Credits2));

        System.out.println("GPA is " + GPA + ".");
    }
}

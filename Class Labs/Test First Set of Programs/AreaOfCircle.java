public class AreaOfCircle {
    public static void main(String[] args) {        
        double radius = 5.8;
        double area;

        area = Math.PI * radius * radius;

        System.out.println("The area of a circle with radius " + radius + " is " + area + ".");
    }
}

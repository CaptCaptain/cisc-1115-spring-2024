public class PrintPrimes {
    public static Boolean isPrime(int num1) {
        int numberOfDivisors = 0;
        Boolean couldBePrimeNumber = false;
        
        for (int k = 1; k < num1 + 1; k++) {
            if ((double) num1 / k == 1 ^ (double) num1 / k == num1) {
                couldBePrimeNumber = true;
            }
            if (num1 % k == 0) {
                numberOfDivisors += 1;
            }
        }

        Boolean isPrimeNumber = couldBePrimeNumber && numberOfDivisors <= 2;

        return isPrimeNumber;
    }

    public static Boolean isLineFull(int numOfPrimesPrinted) {
        Boolean isPastTenNumbers = numOfPrimesPrinted % 10 == 0;

        return isPastTenNumbers;
    }

    public static void main(String[] args) {
        int numberOfPrimeNumbers = 0;
        int currentNumber = 0;
        String currentRowString = "";
        String rowStringFormat = "%5d";

        while (numberOfPrimeNumbers < 50) {
            currentNumber++;

            Boolean isPrimeNumber = isPrime(currentNumber);
            
            if (!isPrimeNumber) {
                continue;
            }

            numberOfPrimeNumbers++;

            currentRowString += String.format(rowStringFormat, currentNumber);

            if (isLineFull(numberOfPrimeNumbers)) {
                currentRowString = currentRowString.substring(1);

                System.out.print(currentRowString);
                System.out.println(" ");

                currentRowString = "";
            }
        }
    }
}

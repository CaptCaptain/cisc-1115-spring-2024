import java.util.*;

public class GreatestCommonDivisor {
    public static int getSmallest(int n1, int n2) {
        return n1 > n2 ? n1 : n2;
    }

    public static Boolean isCommonDivisor(int d, int n1, int n2) {
        return n1 % d == 0 && n2 % d == 0 ? true : false;
    }

    public static int computeGcd(int num1, int num2) {
        for (int k = getSmallest(num1, num2); k > 0; k--) {
            if (isCommonDivisor(k, num1, num2)) return k;
        }
        return 1;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.print("Enter two positive integers: ");

        int num1 = in.nextInt();
        int num2 = in.nextInt();

        System.out.printf("The greatest common divisor of %1$s and %2$s is %3$s" + "\n", num1, num2, computeGcd(num1, num2));
    }
}

import java.util.*;

public class CheckingPalindrome {
    public static Boolean isStringPalindrome(String word) {
        String reverseString = "";

        // Add characters from the input string in reverse.
        for (int i = word.length(); i > 0; i--) {
            reverseString += word.substring(i - 1, i);
        }
        
        Boolean isPalindrone = word.equals(reverseString);

        return isPalindrone;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.print("Enter a string (one string per line): ");

        String inputString = in.nextLine();
        
        Boolean isPalindrone = isStringPalindrome(inputString);

        System.out.printf("Is String <%s> a palindrone? %b", inputString, isPalindrone);
        System.out.println("");
    }
}

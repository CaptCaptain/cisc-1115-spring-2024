import java.util.*;

public class HexadecimalToDecimal {
    public static int getDecimalFromHexDigit(char d) {
        if (d >= '0' && d <= '9') {
            return d - '0';
        } else {
            return d + 15 - 'F';
        }
    }

    public static double convertToDecimal(String hexText) {
        int base = 16;
        double decimal = 0;
        
        for (int i = 0; i < hexText.length(); i++) {
            int exponent = hexText.length() - i - 1;
            int coefficient = getDecimalFromHexDigit(hexText.charAt(i));

            double term = coefficient * Math.pow(base, exponent);

            decimal += term;
        }

        return decimal;
    }

    public static void main(String[] args) {
        System.out.print("Enter a hexadecimal number (no more than 8 digits): ");

        Scanner in = new Scanner(System.in);

        String hexadecimalNumber = in.next();

        System.out.printf("Hexadecimal number %s is decimal number %.0f\n", hexadecimalNumber, convertToDecimal(hexadecimalNumber));
    }
}

import java.util.Scanner;

public class DecimalToHexadecimal {
    public static char getHexFromDec(long n) {
        if (n <= 9) {
            return (char) ('0' + n);
        } else {
            return (char) ('A' + n - 10);
        }
    }

    public static String convertToHexadecimal(long n) {
        int base = 16;

        String hexString = "";

        for (long quotient = n; quotient > 0; quotient /= base) {
            hexString = getHexFromDec(quotient % base) + hexString;
        }

        return hexString;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.print("Enter a decimal number: ");

        long decimal = in.nextLong();

        System.out.printf("Decimal number %s is hexadecimal number %s\n", decimal, convertToHexadecimal(decimal));
    }
}

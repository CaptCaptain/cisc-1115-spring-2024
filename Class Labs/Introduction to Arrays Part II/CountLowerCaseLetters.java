public class CountLowerCaseLetters {
    public static int[] countLowerCaseLetters(char[] letters) {
        int[] occurrencesList = new int[26];
        
        for (int i = 0; i < letters.length; i++) {
            occurrencesList[(int)('a' + letters[i] - 'a') - 'a'] += 1;
        }
        
        return occurrencesList;
    }
}

public class GetLargestElement {
    public static int getLargestElement(double[] numList) {
        int maxIndex = 0;
        for (int i = 0; i < numList.length; i++) {
            if (numList[maxIndex] < numList[i]) {
                maxIndex = i;
            }
        }
        return maxIndex;
    }
}

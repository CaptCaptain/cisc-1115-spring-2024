public class GetSmallestValue {
    public static double getSmallestValue(double[] numList) {
        double min = numList[0];
        for (int i = 0; i < numList.length; i++) {
            if (min > numList[i]) {
                min = numList[i];
            }
        }
        return min;
    }
}

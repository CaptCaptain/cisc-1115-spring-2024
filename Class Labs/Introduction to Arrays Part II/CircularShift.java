public class CircularShift {
    public static void circularShift(int[] arr, int position, char direction) {
        int shift = direction == 'L' ? 1 : (direction == 'R' ? -1 : 0);
        
        int[] tempList = arr.clone();

        for (int i = 0; i < arr.length; i++) {
            int index = (i + Math.min(position, arr.length) * shift);

            index = index < 0 ? arr.length + index :
                    (
                        index >= arr.length ? index - arr.length :
                        index
                    );

            arr[i] = tempList[index];
        }
    }
}

public class ReverseArray {
    public static void reverseArray(int[] arr) {
        int[] tempList = arr.clone();
        
        for (int i = 0; i < tempList.length; i++) {
            arr[tempList.length - i - 1] = tempList[i];
        }
    }
}

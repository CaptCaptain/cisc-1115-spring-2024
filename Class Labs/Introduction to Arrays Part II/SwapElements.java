public class SwapElements {
    public static void swapElements(String[] arr, int i, int j) {
        String temp = arr[i];
        
        arr[i] = arr[j];
        arr[j] = temp;
    }
}

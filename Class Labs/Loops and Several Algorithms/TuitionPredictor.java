import java.util.Scanner;

public class TuitionPredictor {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.print("Enter tuition and annual percentage rate: ");

        int Principal = in.nextInt();
        double APR = in.nextDouble();
        double APRPercentage = APR / 100;
        int Years = 0;
        
        while (true) {
            Years += 1;

            double Tuition = Math.round((double) Principal * Math.pow(1 + APRPercentage, Years));

            if (Tuition > Principal * 2) {
                break;
            }
        }

        System.out.printf("Given tuition %d and APR %.2f%%, the tuition would double in %d years.", Principal, APR, Years);
        System.out.println("");
    }
}

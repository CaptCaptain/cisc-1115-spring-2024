public class MultiplicationTable {
    public static void main(String[] args) {

        // Print Column Label
        System.out.printf("%1s", "");

        for (int x = 0; x < 10; x++) {
            System.out.printf("%3s", x);
        }

        // Print Whitespace
        System.out.println(" ");

        // Generate Multiplication Table
        for (int y = 0; y < 10; y++) {
            // Print Row Label
            System.out.printf("%s", y);
            
            // Print Values for Row
            for (int x = 0; x < 10; x++) {
                System.out.printf("%3s", x * y);
            }

            // Print Whitespace
            System.out.println(" ");
        }
    }
}

public class PrintPrimes {
    public static void main(String[] args) {
        int numberOfPrimeNumbers = 0;
        int currentNumber = 0;
        String currentRowString = "";
        String rowStringFormat = "%5d";

        while (numberOfPrimeNumbers < 50) {
            currentNumber++;

            int numberOfDivisors = 0;
            Boolean couldBePrimeNumber = false;
            
            for (int k = 1; k < currentNumber + 1; k++) {
                if ((double) currentNumber / k == 1 ^ (double) currentNumber / k == currentNumber) {
                    couldBePrimeNumber = true;
                }
                if (currentNumber % k == 0) {
                    numberOfDivisors += 1;
                }
            }

            Boolean isPrimeNumber = couldBePrimeNumber && numberOfDivisors <= 2;
            
            if (!isPrimeNumber) {
                continue;
            }

            numberOfPrimeNumbers++;

            currentRowString += String.format(rowStringFormat, currentNumber);

            Boolean isPastTenNumbers = numberOfPrimeNumbers % 10 == 0;

            if (isPastTenNumbers) {
                currentRowString = currentRowString.substring(1);

                System.out.print(currentRowString);
                System.out.println(" ");

                currentRowString = "";
            }
        }
    }
}

public class PiEstimator {
    public static double CIRCLE_CENTER_X, CIRCLE_CENTER_Y = 1/2;
    public static void main(String[] args) {
        int nC = 0;
        double Pi = 0;

        for (int n = 1; n < 1000000; n++) {
            double randomX = Math.random();
            double randomY = Math.random();

            Boolean pointInCircle = Math.pow((randomX - CIRCLE_CENTER_X), 2) + Math.pow((randomY - CIRCLE_CENTER_Y), 2) < 1;

            if (pointInCircle) {
                nC += 1;
                Pi = 4. * nC / n;
            }
        }

        System.out.printf("The estimated PI is %.2f after 1000000 random points.", Pi);
        System.out.println("");
    }
}

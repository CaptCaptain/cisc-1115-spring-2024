import java.util.*;

public class CheckingPalindrome {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.print("Enter a string (one string per line): ");

        String inputString = in.nextLine();
        String reverseString = "";

        // Add characters from the input string in reverse.
        for (int i = inputString.length(); i > 0; i--) {
            reverseString += inputString.substring(i - 1, i);
        }
        
        Boolean isPalindrone = inputString.equals(reverseString);

        System.out.printf("Is String <%s> a palindrone? %b", inputString, isPalindrone);
        System.out.println("");
    }
}

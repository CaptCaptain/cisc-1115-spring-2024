import java.util.*;

public class PowerNumberPyramid {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.print("Enter height of the pyramid: ");

        int heightOfPyramid = in.nextInt() + 1;
        int widthBetweenNumbers = (int) Integer.toString((int) Math.pow(2, heightOfPyramid) / 4).length() + 1;

        for (int y = 1; y < heightOfPyramid; y++) {
            String rowString = "";
            String rowFormat = "%" + widthBetweenNumbers + "d";

            // Get leading spaces
            String leadingSpaceFormat = "%" + ((heightOfPyramid - y) * (widthBetweenNumbers)) + "s";
            String leadingSpaceString = String.format(leadingSpaceFormat, "");

            // Print Left Side of Pyramid
            rowString += leadingSpaceString;

            for (int x = 0; x < y; x++) {
                int Number = (int) Math.pow(2, x);

                rowString += String.format(rowFormat, Number);
            }

            // Print Right Side of Pyramid
            for (int x = 1; x < y; x++) {
                int Number = (int) Math.pow(2, y - x - 1);

                rowString += String.format(rowFormat, Number);
            }
            rowString += " ";

            // Trim Leading Whitespace
            rowString = rowString.substring(widthBetweenNumbers + 1);

            // Output
            System.out.print(rowString);

            System.out.println("");
        }
    }
}

import java.util.*;

public class GreatestCommonDivisor {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.print("Enter two integers: ");

        int num1 = in.nextInt();
        int num2 = in.nextInt();

        for (int k = Math.max(num1, num2); k > 0; k--) {
            if (num1 % k == 0 && num2 % k == 0) {
                System.out.printf("The greatest common divisor of %1$s and %2$s is %3$s.", num1, num2, k);
                break;
            }
        }
        
        System.out.println("");
    }
}

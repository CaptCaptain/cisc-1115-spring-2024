import java.util.*;

public class AnalyzeNumbers {
    public static double[] getElements(Scanner in, int numberOfElements) {
        double[] numbers = new double[numberOfElements];

        for (int i = 0; i < numberOfElements; i++) {
            numbers[i] = in.nextDouble();
        }

        return numbers;
    }

    public static double getAverageOfNumbers(double[] numbers) {
        double average = 0;

        for (int i = 0; i < numbers.length; i++) {
            average += numbers[i];
        }

        average /= numbers.length;

        return average;
    }

    public static int getNumbersAboveN(double[] numbers, double n) {
        int aboveAverageCount = 0;

        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] > n) {
                aboveAverageCount += 1;
            }
        }

        return aboveAverageCount;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        double[] numbers = getElements(in, in.nextInt());

        double average = getAverageOfNumbers(numbers);

        int aboveAverageCount = getNumbersAboveN(numbers, average);

        if (aboveAverageCount > 0 && aboveAverageCount <= 1) {
            System.out.printf("%d value is above the average of %f\n", aboveAverageCount, average);
        } else if (aboveAverageCount > 0) {
            System.out.printf("%d values are above the average of %f\n", aboveAverageCount, average);
        }
    }
}

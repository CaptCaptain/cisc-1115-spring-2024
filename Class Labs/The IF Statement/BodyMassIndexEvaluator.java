import java.util.Scanner;

public class BodyMassIndexEvaluator {
	public static final double metersToFeetRatio = 0.3048;
	public static final double kgToPoundsRatio = 0.4535924;

	public static final double underWeightBMILimit = 18.5;
	public static final double normalBMILimit = 25.0;
	public static final double overWeightBMILimit = 30.0;
	
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		
		System.out.print("Enter the patient's weight in pounds: ");
		
		int weightInPounds = in.nextInt();
		
		System.out.print("Enter feet for the patient's height: ");
		
		int heightInFeet = in.nextInt();
		
		System.out.print("Enter inches for the patient's height: ");
		
		int heightInInches = in.nextInt();
		
		double I, W, H;
		H = heightInFeet + (heightInInches / 12.);
		H = H * metersToFeetRatio;
		W = weightInPounds * kgToPoundsRatio;
		I = W / Math.pow(H, 2);
		I = (int) (I * 10.) / 10.;
		
		double BMI = I;
		
		String interpretationOfBMI = "Unknown";
		
		if (BMI < underWeightBMILimit) {
			interpretationOfBMI = "Underweight";
		} else if (BMI > underWeightBMILimit && BMI < normalBMILimit) {
			interpretationOfBMI = "Normal";
		} else if (BMI > normalBMILimit && BMI < overWeightBMILimit) {
			interpretationOfBMI = "Overweight";
		} else if (BMI > overWeightBMILimit) {
			interpretationOfBMI = "Obese";
		}
		
		System.out.println("");
		
		System.out.println("BMI Calculation and Interpretation");
		
		System.out.println(String.format(" Patient's BMI: %s", BMI));
		
		System.out.println(String.format("Interpretation: %s", interpretationOfBMI));
	}
}
import java.util.Scanner;

public class FederalIncomeTaxForSingles {
    public static final double FirstBracketTaxRate = 0.10;
    public static final double SecondBracketTaxRate = 0.12;
    public static final double ThirdBracketTaxRate = 0.22;
    public static final double FourthBracketTaxRate = 0.24;
    public static final double FifthBracketTaxRate = 0.32;
    public static final double SixthBracketTaxRate = 0.35;
    public static final double SeventhBracketTaxRate = 0.37;

    public static final int FirstBracketLowerLimit = 0;
    public static final int FirstBracketUpperLimit = 9950;

    public static final int SecondBracketLowerLimit = FirstBracketUpperLimit;
    public static final int SecondBracketUpperLimit = 40525;

    public static final int ThirdBracketLowerLimit = SecondBracketUpperLimit;
    public static final int ThirdBracketUpperLimit = 86375;

    public static final int FourthBracketLowerLimit = ThirdBracketUpperLimit;
    public static final int FourthBracketUpperLimit = 164925;

    public static final int FifthBracketLowerLimit = FourthBracketUpperLimit;
    public static final int FifthBracketUpperLimit = 209425;

    public static final int SixthBracketLowerLimit = FifthBracketUpperLimit;
    public static final int SixthBracketUpperLimit = 523600;

    public static final int SeventhBracketLowerLimit = SixthBracketUpperLimit;

    public static final double FirstBracket = (FirstBracketUpperLimit) * FirstBracketTaxRate;
    public static final double SecondBracket = FirstBracket + (SecondBracketUpperLimit - SecondBracketLowerLimit) * SecondBracketTaxRate;
    public static final double ThirdBracket = SecondBracket + (ThirdBracketUpperLimit - ThirdBracketLowerLimit) * ThirdBracketTaxRate;
    public static final double FourthBracket = ThirdBracket + (FourthBracketUpperLimit - FourthBracketLowerLimit) * FourthBracketTaxRate;
    public static final double FifthBracket = FourthBracket + (FifthBracketUpperLimit - FifthBracketLowerLimit) * FifthBracketTaxRate;
    public static final double SixthBracket = FifthBracket + (SixthBracketUpperLimit - SixthBracketLowerLimit) * SixthBracketTaxRate;

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		
		System.out.println("Federal Tax Calculation for Singles");
		
		System.out.print("Enter adjusted net income ($): ");
		
		double adjustedNetIncome = in.nextDouble();
        double federalIncomeTax = 0;

        if (adjustedNetIncome < FirstBracketUpperLimit) {

            federalIncomeTax = adjustedNetIncome * FirstBracketTaxRate;

        } else if (adjustedNetIncome < SecondBracketUpperLimit) {

            federalIncomeTax = FirstBracket + (adjustedNetIncome - SecondBracketLowerLimit) * SecondBracketTaxRate;

        } else if (adjustedNetIncome < ThirdBracketUpperLimit) {

            federalIncomeTax = SecondBracket + (adjustedNetIncome - ThirdBracketLowerLimit) * ThirdBracketTaxRate;

        } else if (adjustedNetIncome < FourthBracketUpperLimit) {

            federalIncomeTax = ThirdBracket + (adjustedNetIncome - FourthBracketLowerLimit) * FourthBracketTaxRate;

        } else if (adjustedNetIncome < FifthBracketUpperLimit) {

            federalIncomeTax = FourthBracket + (adjustedNetIncome - FifthBracketLowerLimit) * FifthBracketTaxRate;

        } else if (adjustedNetIncome < SixthBracketUpperLimit) {

            federalIncomeTax = FifthBracket + (adjustedNetIncome - SixthBracketLowerLimit) * SixthBracketTaxRate;

        } else if (adjustedNetIncome > SixthBracketUpperLimit) {

            federalIncomeTax = SixthBracket + (adjustedNetIncome - SeventhBracketLowerLimit) * SeventhBracketTaxRate;
            
        }

        federalIncomeTax = Math.floor(federalIncomeTax);

        System.out.println(String.format("For adjusted income of $%s, the federal income tax is $%s.", (int) adjustedNetIncome, (int) federalIncomeTax));
	}
}
public class WeightedAverage {
    public static void main(String[] args) {
        java.util.Scanner in = new java.util.Scanner(System.in);

        System.out.println("Enter two numbers and two weights: ");

        double x1 = in.nextDouble();
        double x2 = in.nextDouble();
        double w1 = in.nextDouble();
        double w2 = in.nextDouble();
        double weightAverage = (w1 * x1) + (w2 * x2);

        System.out.printf("The weighted average is %s", weightAverage);

        System.out.println("");
    }
}
public class EvenOrOdd {
    public static void main(String[] args) {
        int number = (int) (Math.random() * 100);

        if (number % 2 == 0) {
            System.out.printf("Number %s is even", number);
        } else {
            System.out.printf("Number %s is odd", number);
        }
    }
}

public class SumNumbers {
    public static int GetSumBetween(int num1, int num2) {
        int sum = 0;
        
        for (int i = num1; i <= num2; i++) {
            sum += i;
        }

        return sum;
    }

    public static void main(String[] args) {
        System.out.println(GetSumBetween(1, 10) + GetSumBetween(20, 30) + GetSumBetween(35, 45));
    }
}

import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

public class UpperToLower {
    public static void main(String[] args) throws FileNotFoundException, UnsupportedEncodingException {
        String inFile = args[0];
        String outFile = args[1];

        upperToLower(inFile, outFile);
    }

    public static void upperToLower(String inFile, String outFile) throws FileNotFoundException, UnsupportedEncodingException {
        String[] lines = getAllLines(inFile);

        saveToFile(lines, outFile);
    }

    public static void saveToFile(String[] lines, String outFile) throws FileNotFoundException, UnsupportedEncodingException {
        PrintWriter out = null;
        
        try {
            out = new PrintWriter(new File(outFile), "UTF-8");
            
            for (int i = 0; i < lines.length; i++) {
                out.println(lines[i].toLowerCase());
            }
        } finally {
            if (out != null) out.close();
        }
    }

    public static String[] getAllLines(String inFilePath) throws FileNotFoundException {
        Scanner in = null;
        
        try {
            in = new Scanner(new File(inFilePath), "UTF-8");
            
            int count = getNumberOfLines(inFilePath);
            
            String[] resultList = new String[count];
            
            for (int i = 0; i < resultList.length; i++) {
                resultList[i] = in.nextLine();
            }
            
            return resultList;
        } finally {
            if (in != null) in.close();
        }
    }

    public static int getNumberOfLines(String inFilePath) throws FileNotFoundException {
        Scanner in = null;
        
        try {
            in = new Scanner(new File(inFilePath), "UTF-8");
            
            int count = 0;
            
            while (in.hasNext()) {
                in.nextLine();
                count++;
            }
            
            return count;
        } finally {
            if (in != null) in.close();
        }
    }
}

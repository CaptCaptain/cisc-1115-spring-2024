public class Increment {
    public static int IncrementAndReturn(int n) {
        n++;

        System.out.printf("n in method is: %s" + "\n", n);
        
        return n;
    }

    public static void IncrementValue(int n) {
        System.out.printf("n in method is: %s" + "\n", n);
    }

    public static void main(String[] args) {
        // Increment Value
        int n = 1;

        System.out.println("Increment by Value");

        System.out.printf("Before method invocation n is: %s" + "\n", n);

        IncrementValue(n);

        System.out.printf("After method invocation n is: %s" + "\n", n);

        System.out.println("---------------------------");

        // Increment and Return
        n = 1;

        System.out.println("Increment Value and Return");

        System.out.printf("Before method invocation n is: %s" + "\n", n);

        n = IncrementAndReturn(n);

        System.out.printf("After method invocation n is: %s" + "\n", n);
    }
}

public class BubbleSort {
    public static void main(String[] args) {
        int[] arr = randomArray((int) (Math.random() * 10));

        printArray(arr);

        System.out.println("--------------");

        bubbleSort(arr);

        printArray(arr);
    }

    private static int[] randomArray(int arraySize) {
        int[] arr = new int[arraySize];

        for (int i = 0; i < arr.length; i++) {
            arr[i] = (int) (Math.random() * 100);
        }

        return arr;
    }

    public static void printArray(int[] numList) {
        for (int i = 0; i < numList.length; i++) {
            System.out.printf("%s[%d] = %d\n", "arr", i, numList[i]);
        }
    }

    public static void swap(int[] numList, int i, int j) {
        int temp = numList[i];

        numList[i] = numList[j];
        numList[j] = temp;
    }

    public static void bubbleSort(int[] numList) {
        for (int endIndex = numList.length - 1; endIndex >= 0; endIndex--) {
            bubbleUp(numList, endIndex);
        }
    }

    public static void bubbleUp(int[] numList, int endIndex) {
        for (int i = 0; i < endIndex; i++) {
            if (numList[i] > numList[i + 1]) swap(numList, i, i + 1);
        }
    }
}

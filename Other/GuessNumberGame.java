import java.util.*;

public class GuessNumberGame {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        int number = (int) (Math.random() * 101);
        int guess = -1;
        
        while (true) {
            System.out.print("Enter an integer between 0-100: ");

            guess = in.nextInt();

            if (guess == number) {
                break;
            } else if (guess > number) {
                System.out.println("Too high!");
            } else if (guess < number) {
                System.out.println("Too low!");
            }
        }

        System.out.println("Correct!");
    }
}
import java.util.Scanner;

public class FiveOrEven {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        int number = in.nextInt();

        Boolean isMultipleOfFive = (number % 5 == 0);
        Boolean isDivisibleByTwo = (number % 2 == 0);

        if (isMultipleOfFive) {
            System.out.println("HiFive");
        }

        if (isDivisibleByTwo) {
            System.out.println("HiEven");
        }
    }
}

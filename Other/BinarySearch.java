public class BinarySearch {
    public static int binarySearch(int[] numList, int key) {
        if (numList == null) {
            return -1;
        }

        if (numList.length == 0) {
            return -1;
        }

        int low = 0;
        int high = numList.length - 1;

        while (true) {
            int mid = (low + high) / 2;

            if (key < numList[mid]) {
                high = mid - 1;
            } else if (key > numList[mid]) {
                low = mid + 1;
            } else if (key == numList[mid]) {
                return mid;
            } else {
                return -1;
            }

            System.out.printf("%s, %s, %s\n", low, mid, high);
        }
    }

    public static void main(String[] args) {
        System.out.println(binarySearch(new int[] {1, 1, 3, 4, 4}, 5));
    }
}

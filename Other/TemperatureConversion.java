public class TemperatureConversion {
    public static void main(String[] args) {
        try (java.util.Scanner in = new java.util.Scanner(System.in)) {
            double Celsius = in.nextDouble();

            System.out.println((Celsius * 9/5) + 32);
        }
    }
}

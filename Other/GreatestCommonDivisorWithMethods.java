import java.util.*;

public class GreatestCommonDivisorWithMethods {
    /*
     * Return if n is a positive integer
     * 
     * @parameter n
     * @return Boolean
     */
    public static Boolean isPositiveInteger(int n) {
        return n >= 0 ? true : false;
    }

    /*
     * Get Greatest Common Divisor of num1 and num2
     * 
     * @parameter num1
     * @parameter num2
     * @return Greatest Common Divisor
     */
    public static int GetGCD(int num1, int num2) {
        for (int k = Math.max(num1, num2); k > 0; k--) {
            if (num1 % k == 0 && num2 % k == 0) return k;
        }
        return 1;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.print("Enter two positive integers: ");

        int num1 = in.nextInt();
        int num2 = in.nextInt();

        if (!isPositiveInteger(num1) || !isPositiveInteger(num2)) {
            System.out.printf("%1$s or %2$s are not positive integers.", num1, num2);
            return;
        }

        System.out.printf("The greatest common divisor of %1$s and %2$s is %3$s." + "\n", num1, num2, GetGCD(num1, num2));
    }
}

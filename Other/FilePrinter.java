import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class FilePrinter {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner in = null;

        try {
            in = new Scanner(new File("./Files/Number.txt"), "UTF-8");

            double sum = 0;
            int count = 0;
            
            while (in.hasNext()) {
                double number = in.nextDouble();

                sum += number;
                count ++;

                System.out.println(number);
            }

            System.out.printf("The sum of the following numbers is: %f\n", sum);
            System.out.printf("The average of the following numbers is: %f\n", sum / count);
        } finally {
            if (in != null) in.close();
        }
    }
}

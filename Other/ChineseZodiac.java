public class ChineseZodiac {
    public static String[] ZodiacSigns = {"Monkey", "Rooster", "Dog", "Pig", "Rat", "Ox", "Tiger", "Rabbit", "Dragon", "Snake", "Horse", "Sheep"};
    public static void main(String[] args) {
        java.util.Scanner in = new java.util.Scanner(System.in);

        System.out.print("Enter birth year: ");

        int year = in.nextInt();
        int zodiacYear = year % 12;

        System.out.print("Your Chinese zodiac is: " + ZodiacSigns[zodiacYear]);
    }
}

public class HexToDex {
    public static String HEX_CHARS = "0123456789ABCDEF";
    public static void main(String[] args) {
        java.util.Scanner in = new java.util.Scanner(System.in);

        System.out.print("Enter a hexadecimal sequence: ");

        String hexadecimal = (String) in.next();
        int decimal = 0;

        for (int i = 0; i < hexadecimal.length(); i++) {
            int number = HEX_CHARS.indexOf(hexadecimal.charAt(i)) * (int) Math.pow(16., hexadecimal.length() - 1 - i);

            decimal += number;
        }

        System.out.println(String.format("The decimal number is: %s", decimal));
    }
}

import java.util.Arrays;

public class Merge {
    public static int[] merge(int[] arr1, int[] arr2) {
        int[] mergedArr = new int[arr1.length + arr2.length];
        
        for (int i = 0; i < mergedArr.length; i++) {
            if (i < arr1.length) {
                mergedArr[i] = arr1[i];
            } else {
                mergedArr[i] = arr2[i - mergedArr.length + arr2.length];
            }
        }

        Arrays.sort(mergedArr);
        
        return mergedArr;
    }

    public static void main(String[] args) {
        int[] arr = new int[] {1,2,3,4};
        
        for (int i = 0; i < arr.length / 2; i++) {
            int temp = arr[i];
            arr[i] = arr[arr.length - i - 1];
            arr[arr.length - i - 1] = temp;
        }

        for (int i : arr) {
            System.out.println(i);
        }
    }
}

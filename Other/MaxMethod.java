public class MaxMethod {
    public static void main(String[] args) {
        int n1 = (int) (Math.random() * 10);
        int n2 = (int) (Math.random() * 10);
        int mn = max(n1, n2);

        System.out.printf("max(%d, %d) -> %d\n", n1, n2, mn);

        double d1 = (Math.random() * 10);
        double d2 = (Math.random() * 10);
        double md = max(d1, d2);

        System.out.printf("max(%f, %f) -> %f\n", d1, d2, md);

        String s1 = Math.random() > 0.5 ? "ABC" : "CBA";
        String s2 = Math.random() > 0.5 ? "ABC" : "CBA";
        String ms = max(s1, s2);

        System.out.printf("max(\"%s\", \"%s\") -> \"%s\"\n", s1, s2, ms);

        char c1 = Math.random() > 0.5 ? 'A' : 'C';
        char c2 = Math.random() > 0.5 ? 'A' : 'C';
        char mc = max(c1, c2);

        System.out.printf("max(%c, %c) -> %c\n", c1, c2, mc);
    }

    public static int max(int a, int b) {
        return a > b ? a : b;
    }

    public static double max(double a, double b) {
        return a > b ? a : b;
    }

    public static String max(String a, String b) {
        return a.compareTo(b) > 0 ? a : b;
    }

    public static char max(char a, char b) {
        return a > b ? a : b;
    }
}

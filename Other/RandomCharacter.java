public class RandomCharacter {
    public static void main(String[] args) {
        System.out.print(randomDigits());
    }

    public static char randomLowerCaseLetter() {
        int n = (int)(Math.random() * 26);
        char c = (char)('a' + n);
        return c;
    }

    public static char randomUpperCaseLetter() {
        int n = (int)(Math.random() * 26);
        char c = (char)('A' + n);
        return c;
    }

    public static int randomDigit() {
        return (int) (Math.random() * 10);
    }

    public static char randomLetter(boolean upperCase) {
        if (upperCase) return randomUpperCaseLetter(); else return randomLowerCaseLetter();
    }

    public static Boolean randomBoolean() {
        return Math.random() > 0.5 ? true : false;
    }

    public static String randomDigits() {
        String s = "";

        for (int i = 0; i < 9; i++) {
            s += randomDigit();
        }

        return s;
    }

    public static String randomLetterString(int length) {
        String s = "";

        for (int i = 0; i < length; i++) {
            s += randomLetter(randomBoolean());
        }

        return s;
    }
}

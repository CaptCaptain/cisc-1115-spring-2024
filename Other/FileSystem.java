import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

public class FileSystem {
    public static void main(String[] args) {
        try {
            Writer();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static void FileDemo() throws IOException  {
        File f = new File("./Files/Text.txt");

        if (f.exists()) {
            System.out.println(f.length());
        }
    }

    public static void Writer() throws FileNotFoundException, UnsupportedEncodingException {
        PrintWriter out = null;

        try {
            out = new PrintWriter(new File("./Files/Output.txt"), "UTF-8");
            out.println("Hello, World!");
            out.println("Hello, Java!");
            out.close();
        } finally {
            if (out != null) out.close();
        }
    }
}

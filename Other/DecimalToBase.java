import java.lang.*;
import java.io.*;
import java.util.*;

public class DecimalToBase {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        int number = in.nextInt();
        String binarySequence = "";
        String reverseString = "";

        while (number != 0) {
            binarySequence += number % 2;
            number /= 2;
        }

        for (int i = 0; i < binarySequence.length(); i++) {
            reverseString += binarySequence.charAt(binarySequence.length() - i - 1);
        }

        System.out.print(reverseString);
    }
}

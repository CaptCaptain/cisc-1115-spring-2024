import java.util.*;

public class SumCalculator {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        int sum = 0;

        Boolean moreNumbersToAdd = true;
        while (moreNumbersToAdd) {
            System.out.print("Enter a number: ");

            int number = in.nextInt();

            sum += number;

            Boolean waitingForInput = true;
            while (waitingForInput) {
                System.out.println("Do you want to enter more numbers? (y/n): ");

                String userResponse = in.next();

                switch (userResponse.toLowerCase()) {
                    case "y":
                        waitingForInput = false;
                        break;
                    case "n":
                        moreNumbersToAdd = false;
                        waitingForInput = false;
                        break;
                    default:
                        System.out.println("Invalid input.");
                        break;
                }
            }
        }

        System.out.println(String.format("The sum is %s.", sum));
    }
}

import java.util.Scanner;

public class SubtractionQuiz {
    public static void main(String[] args) {
        int num1 = (int)(Math.random() * 10);
        int num2 = (int)(Math.random() * num1);
        int expected = num1 - num2;

        System.out.print(String.format("What is %s - %s? ", num1, num2));

        Scanner in = new Scanner(System.in);
        int answer = in.nextInt();

        if (answer == expected) {
            System.out.println("Correct!");
        } else {
            System.out.println("Incorrect!");
        }
    }
}

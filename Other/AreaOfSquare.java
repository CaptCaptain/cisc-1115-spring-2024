import java.util.*;
import java.io.*;

class AreaOfSquare {
	public static void main(String [] args) throws Exception {
		Scanner stdin = new Scanner(System.in);

		double areaOfSquare = 0.5;
		
        areaOfSquare = Math.sqrt(stdin.nextDouble());
		
		if (areaOfSquare == 0) {
			System.out.println("INVALID");
		} else {
			System.out.println(areaOfSquare);
		}
	}


}

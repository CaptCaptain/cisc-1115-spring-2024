import java.util.*;

class AdditionQuestion {
    public int num1;
    public int num2;

    public AdditionQuestion() {
        num1 = (int) (Math.random() * 10);
        num2 = (int) (Math.random() * 10);
    }

    public int GetAnswer() {
        return num1 + num2;
    }

    public String GetString() {
        return num1 + " + " + num2;
    }
}

public class TestGenerator {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        int numberOfQuestions = 5;
        int numberOfCorrectQuestions = 0;

        for (int n = 0; n < numberOfQuestions; n++) {
            AdditionQuestion question = new AdditionQuestion();
            int answer = question.GetAnswer();
            String problem = question.GetString();

            System.out.print(String.format("What is %s? ", problem));

            int submittedAnswer = in.nextInt();

            if (answer == submittedAnswer) {
                numberOfCorrectQuestions += 1;

                System.out.println("Correct!");
            } else {
                System.out.println("Incorrect!");
            }
        }
        
        System.out.println(String.format("Your score is: %s%%", (int) (((double) numberOfCorrectQuestions / numberOfQuestions) * 100)));
    }
}

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Scanner;

public class ScoreAnalyzer {
    public static void main(String[] args) throws FileNotFoundException, UnsupportedEncodingException {
        String[] recordList = loadData("./Files/BasketballPlayers.txt");

        int totalScore = computeTotalScore(recordList);
        double averageScore = (double) totalScore / recordList.length;
        String[] resultList = getPlayersAboveAverageScore(recordList, averageScore);

        System.out.println("The average score is " + averageScore);
        printResult(resultList);
        saveResultList("BestPlayers", resultList);
    }

    public static void saveResultList(String fileName, String[] resultList) throws FileNotFoundException, UnsupportedEncodingException {
        PrintWriter out = null;

        try {
            out = new PrintWriter(new File("./Files/" + fileName + ".txt"), "UTF-8");

            for (int i = 0; i < resultList.length; i++) {
                out.println(resultList[i]);
            }
        } finally {
            if (out != null) out.close();
        }
    }

    public static void printResult(String[] resultList) {
        System.out.println("The players who score above or equal to the average are: ");

        for (int i = 0; i < resultList.length; i++) {
            System.out.printf("%s with a score of %d\n", getPlayer(resultList[i]), getScore(resultList[i]));
        }
    }

    public static String[] getPlayersAboveAverageScore(String[] recordList, double averageScore) {
        int numberOfPlayers = getNumberOfPlayersAboveAverageScore(recordList, averageScore);
        String[] resultList = new String[numberOfPlayers];

        int count = 0;
        for (int i = 0; i < resultList.length; i++) {
            int score = getScore(recordList[i]);

            if (score >= averageScore) {
                resultList[count] = recordList[i];
                count++;
            }
        }

        return resultList;
    }

    public static int getNumberOfPlayersAboveAverageScore(String[] recordList, double averageScore) {
        int count = 0;
        for (int i = 0; i < recordList.length; i++) {
            int score = getScore(recordList[i]);

            if (score >= averageScore) {
                count++;
            }
        }

        return count;
    }

    public static int computeTotalScore(String[] recordList) {
        int total = 0;

        for (int i=0; i<recordList.length; i++) {
            total += getScore(recordList[i]);
        }

        return total;
    }

    public static int getFileSize(String file) throws FileNotFoundException {
        Scanner in = null;

        try {
            in = new Scanner(new File(file));

            int count = 0;

            while (in.hasNextLine()) {
                in.nextLine();
                count++;
            }

            return count;
        } finally {
            if (in != null) in.close();
        }
    }

    public static String[] loadData(String file) throws FileNotFoundException {
        int numberOfRecords = getFileSize(file);
        String[] recordList = new String[numberOfRecords];
        Scanner in = null;

        try {
            in = new Scanner(new File("./Files/BasketballPlayers.txt"));

            int i = 0;

            while (in.hasNext()) {
                String line = in.nextLine();
                recordList[i] = line;
                i++;
            }
        } finally {
            if (in != null) in.close();
        }
        
        return recordList;
    }

    public static int getScore(String record) {
        String[] headers = record.split(",");

        return Integer.parseInt(headers[2]);
    }

    public static String getPlayer(String record) {
        String[] headers = record.split(",");

        return headers[0];
    }
}
